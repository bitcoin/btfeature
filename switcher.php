<?php
# security protection
defined('_we_are_one') || die('Direct access not allowed.');
require 'errors.php';

function switcher($page, $loggedin)
{
    global $abspath;
    try {
        switch($page) {
            case 'login':
                if (!$loggedin)
                    include("$abspath/login.php");
                else
                    log_badpage($page);
                break;

            case 'propose':
                include("$abspath/propose.php");
                break;

            case 'help':
                include("$abspath/help.php");
                break;

            case '':
                include("$abspath/index.php");
                break;  

            default:
                log_badpage($page);
                break;
        }
    } 
    catch (Error $e) {
        report_exception($e, SEVERITY::ERROR);
        # Same as below, but flag + log this for review,
        echo "<div class='content_box'><h3>{$e->getTitle()}</h3>";
        echo "<p>{$e->getMessage()}</p></div>";
    }
    catch (Problem $e) {
        echo "<div class='content_box'><h3>{$e->getTitle()}</h3>";
        echo "<p>{$e->getMessage()}</p></div>";
    }
}

?>

