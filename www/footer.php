    </div>

<?php
if (isset($_SESSION['uid']) && $_SESSION['uid'])
    $loggedin = true;
else
    $loggedin = false;
?>

    <div id='links'>
        <ul>
            <li><a href='?page='>Home</a>
            Return home</li>
<?php
    if ($loggedin) {
?>
            <li><a href='?page=logout'>Logout</a>
            Remove this session</li>
            <li><a href='?page=propose'>Propose</a>
            Propose a feature</li>
<?php } else { ?>
            <li><a href='?page=login'>Login</a>
            Begin here</li>
<?php } ?>
            <li><a href='?page=help'>Help</a>
            How it works</li>
        </ul>
    </div>
        <div id='footer'>
        <p>Thanks to fabianjr, dissipate &amp; genjix for this site!</p>
        </div>
</body>
</html>

