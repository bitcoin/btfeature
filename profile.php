<?php
require 'db.php';

if (!isset($_SESSION['uid']))
    throw new Error('Denied', 'Go away.');

$uid = $_SESSION['uid'];
$oidlogin = $_SESSION['oidlogin'];

if (isset($_POST['new_nickname'])) {
    $newnick = post('new_nickname');

    # check whether nick already exists first
    $query = "
        SELECT 1
        FROM users
        WHERE nickname='$newnick' AND uid!='$uid';
        ";
    $result = do_query($query);
    if (has_results($result)) {
        ?>
        <div class='content_box'>
            <h3>Unfortunately</h3>
            <p>That nickname has already been claimed by another user.</p>
        </div>
        <?php
    }
    else {
        $query = "
            UPDATE users
            SET nickname='$newnick'
            WHERE uid='$uid';
            ";
        do_query($query);
        ?>
        <div class='content_box'>
            <h3>Done</h3>
            <p>Nickname successfully changed.</p>
        </div>
        <?php
    }
}

$query = "
    SELECT
        nickname,
        balance,
        DATE_FORMAT(timest, '%H%i %d/%m/%y') AS timest
    FROM users
    WHERE uid='$uid';
    ";
$result = do_query($query);
$row = get_row($result);

?>
<div class='content_box'>
<h3>Private user info</h3>
<p>All user accounts are private, but the info page for all companies are public.</p>
<p>You are logged in.</p>
<?php
echo '<p>User ID: '.$uid.'</p>';
echo '<p>OpenID: '.$oidlogin.'</p>';
echo "<p>Balance: {$row['balance']}</p>";
echo "<p>Account was created {$row['timest']}.</p>";
if (!isset($row['nickname'])) {
    echo "<p class='warning'>Please set a username for people to reference you by.</p>";
    $nickname = '';
}
else
    $nickname = $row['nickname'];
?>
<p>
<form action='' method='post'>
    <input type='text' name='new_nickname' value='<?php echo $nickname; ?>' />
    <input type='submit' value='Submit' />
</form>
</p>
</div>
<?php
$query = "
    SELECT compid, compname
    FROM companies
    WHERE own_uid='$uid';
    ";
$result = do_query($query);
$first = true;
while ($row = mysql_fetch_assoc($result)) {
    $compid = $row['compid'];
    $compname = $row['compname'];
    if ($first) {
        $first = false;
        echo "<div class='content_box'>";
        echo '<p>Companies: <ul>';
    }
    echo "<li><a href='?page=view_company&compid=$compid'>$compname</a></li>\n";
}
if (!$first)
    echo '</p></ul></div>';
?>

