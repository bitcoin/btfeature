<?php
require 'util.php';

if (isset($_POST['new_contract'])) {
    throw new Error("hello", "hello");
}
else {
    if (!isset($_GET['contid']))
        throw new Problem("Select contract", "No contract was selected to view.");
    $contid = get('contid');
}


$query = "
    SELECT
        contracts.contname AS contname,
        companies.compname AS compname,
        contracts.text AS text,
        DATE_FORMAT(contracts.timest, '%H%i %d/%m/%y') AS timest
    FROM contracts
    JOIN companies
    ON companies.compid=contracts.contid
    WHERE contracts.contid='$contid';
    ";
$result = do_query($query);
$row = mysql_fetch_assoc($result);
if (!$row)
    throw new Problem("Invalid contract", "The contract specified doesn't exist.");
$contname = $row['contname'];
$compname = $row['compname'];
$text = $row['text'];
$timest = $row['timest'];
?>
<div class='content_box'>
<h3>Contract - <?php echo $contname; ?></h3>
<p>Issued by <?php echo "$compname at $timest"; ?></p>
<p><pre><?php echo $text; ?></pre></p>
</div>

