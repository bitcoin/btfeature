<?php
require 'util.php';

if (isset($_SESSION['uid']))
    $uid = $_SESSION['uid'];
else
    $uid = false;

if (isset($_GET['id']))
    $pid = $_GET['id'];
else
    $pid = 'new';

if (isset($_POST['update'])) {
    $short = post('short');
    $info = post('info');
    $query = "
        SELECT 1
        FROM proposals
        WHERE pid='$pid' AND ownid='$uid';
        ";
    $result = do_query($query);
    if (!has_results($result)) {
        $bitcoin = connect_bitcoin();
        $address = $bitcoin->getnewaddress();
        $query = "
            INSERT INTO proposals(
                ownid,
                shortdes,
                info,
                address
            ) VALUES (
                '$uid',
                '$short',
                '$info',
                '$address');
            ";
        do_query($query);
        ?><div class='content_box'>
        <h3>New!</h3>
        <p>Your new proposal has been created.</p></div><?php
        $pid = mysql_insert_id();
    }
    else if ($pid != 'new') {
        $query = "
            UPDATE proposals
            SET
                shortdes='$short',
                info='$info'
            WHERE pid='$pid' AND ownid='$uid';
            ";
        do_query($query);
        ?><div class='content_box'>
        <h3>Updated</h3>
        <p>Proposal has been updated.</p></div><?php
    }
    else {
        throw new Error('Invalid...', "Shouldn't be here.");
    }
}

if ($pid != 'new') {
    $query = "
        SELECT
            ownid,
            shortdes,
            info,
            address,
            DATE_FORMAT(timest, '%H%i %d/%m/%y') AS timest
        FROM proposals
        WHERE pid='$pid';
        ";
    $result = do_query($query);
    if (!has_results($result))
        throw new Problem('Invalid proposal', 'The proposal you specified is invalid!');
    $row = get_row($result);
    $ownid = $row['ownid'];
    $short = $row['shortdes'];
    $info = $row['info'];
    $timest = $row['timest'];
    $address = $row['address'];
}
else {
    $ownid = '';
    $short = '';
    $info = '';
    $timest = '';
    $address = '';
}

?>
<div class='content_box'>
<?php if ($pid != 'new') { ?>
    <p>ID: <?php echo $pid; ?></p>
    <p>Created: <?php echo $timest; ?></p>
    <p>Donate to <?php echo $address; ?></p>
<?php } ?>

<?php if ($pid == 'new' || ($uid != false && $uid == $ownid)) { ?>
<p><form action='?page=propose&id=<?php echo $pid; ?>' method='post'>
    <input type='text' name='short' value='<?php echo $short; ?>' />
    <textarea name='info'><?php echo $info; ?></textarea>
    <input type='hidden' name='update' value='yes' />
    <?php if ($pid == 'new') { ?>
        <input type='submit' value='Submit' />
    <?php } else { ?>
        <input type='submit' value='Update' />
    <?php } ?>
</form></p>
<?php } else { ?>
    <p>Description: <?php echo $short; ?></p>
    <pre><?php echo $info; ?></pre>
<?php } ?>
</div>

