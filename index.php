<?php
require 'util.php';

$uid = false;
if (isset($_SESSION['uid']))
    $uid = $_SESSION['uid'];

$bitcoin = connect_bitcoin();

$query = "SELECT pid, ownid, shortdes, address, timest, UNIX_TIMESTAMP(timest) AS posix FROM proposals ORDER BY timest DESC;";
$result = do_query($query);

$addresses_unformatted = $bitcoin->listreceivedbyaddress();
$addresses = array();
foreach ($addresses_unformatted as $addyblock) {
    $addy = $addyblock['address'];
    $balance = $addyblock['amount'];
    $addresses[$addy] = $balance;
}

$proposals = array();
while ($row = mysql_fetch_assoc($result)) {
    if (array_key_exists($row['address'], $addresses)) {
        $row['balance'] = (int)$addresses[$row['address']];
    }
    else {
        $row['balance'] = 0;
    }
    $proposals[] = $row;
}

$disp_proposals = array();
for ($i = 0; $i < 10 && $i < sizeof($proposals); $i++) {
    $disp_proposals[] = $proposals[$i];
}

# sort by balance
function cmp_lambda($pa, $pb)
{
    if ($pa['balance'] == $pb['balance'])
        return 0;
    return ($pa['balance'] < $pb['balance']) ? 1 : -1;
}
usort($proposals, "cmp_lambda");
$total = 20;
for ($i = 0; $i < $total && $i < sizeof($proposals); $i++) {
    if (in_array($proposals[$i], $proposals)) {
        $total++;
        continue;
    }
    $disp_proposals[] = $proposals[$i];
}

shuffle($proposals);

?>
<div class='content_box'>
<p>
<table id='prop'>
<tr>
    <th>Description</th>
    <th>Donate!</th>
    <th>Bounty</th>
    <th>Proposed</th>
</tr>
<?php
echo $on = false;
foreach ($disp_proposals as $pr) {
    if ($on)
        echo '  <tr class="alt">';
    else
        echo '  <tr>';
    $on = !$on;
    $balance = internal_to_numstr($pr['balance']);
    echo "      <td><a href='?page=propose&id={$pr['pid']}'>{$pr['shortdes']}</a></td>\n";
    echo "      <td>{$balance}</td>\n";
    echo "      <td>{$pr['address']}</td>\n";
    echo "      <td>{$pr['timest']}</td>\n";
    echo '  </tr>';
}
?>
</table>

