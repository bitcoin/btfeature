<?php
require 'util.php';

if (!isset($_GET['compid']))
    throw new Problem("Select company", "No company was selected to view.");
$compid = get('compid');

if (isset($_POST['update'])) {
    $uid = user_id();
    $query = "
        SELECT 1
        FROM companies
        WHERE own_uid='$uid' AND compid='$compid';
        ";
    $result = do_query($query);
    if (!has_results($result))
        throw Error("Denied", "You don't have permission to edit that company.");
    $new_compname = post("compname");
    # see if compname already exists for another company
    $query = "
        SELECT 1
        FROM companies
        WHERE compname='$new_compname' AND compid!='$compid'
        LIMIT 1;
        ";
    $result = do_query($query);
    if ($new_compname == '')
        echo "<div class='content_box'><h3>Empty</h3><p>Your new company name is empty. Type another name and re-submit.</p></div>";
    else if (has_results($result))
        echo "<div class='content_box'><h3>Name in use</h3><p>Name already used by another company. Choose another.</p></div>";
    else {
        $new_info = mysql_real_escape_string(post('info'));
        $query = "
            UPDATE companies
            SET compname='$new_compname', info='$new_info'
            WHERE compid='$compid';
            ";
        do_query($query);
    }
}

$query = "
    SELECT own_uid, compname, info, timest
    FROM companies
    WHERE compid='$compid';
    ";
$result = do_query($query);
$row = mysql_fetch_assoc($result);
if (!$row)
    throw new Problem("Invalid contract", "The contract specified doesn't exist.");
$own_uid = $row['own_uid'];
$compname = $row['compname'];
$info = $row['info'];
$timest = $row['timest'];

if (isset($_SESSION['uid']) && $_SESSION['uid'] == $own_uid) {
    ?>
    <div class='content_box'>
    <p>Company ID: <?php echo $compid; ?></p>
    <p>Created: <?php echo $timest; ?></p>
    <p>
    <form action='' method='post'>
        <input type='hidden' name='update' value='true' />
        <label for='compname'>Company name:</label>
        <input type='text' name='compname' value='<?php echo $compname; ?>' />
        <label for='info'>Company statement:</label>
        <textarea name='info'><?php echo $info; ?></textarea>
        <input type='submit' value='Update' />
    </form>
    </p>
    <?php
}
else {
    ?>
    <div class='content_box'>
    <p>Company ID: <?php echo $compid; ?></p>
    <p>Name: <?php echo $compname; ?></p>
    <p>Created: <?php echo $timest; ?></p>
    <p>Info:<pre><?php echo $info; ?></pre></p>
    <?php
}
echo "</div>";

$query = "
    SELECT
        contid,
        contname,
        DATE_FORMAT(timest, '%H%i %d/%m/%y') AS timest
    FROM contracts
    WHERE compid='$compid';
    ";
$result = do_query($query);
$first = true;
while ($row = mysql_fetch_assoc($result)) {
    $contid = $row['contid'];
    $contname = $row['contname'];
    $timest = $row['timest'];
    if ($first) {
        $first = false;
        ?><div class='content_box'>
            <h3>Issued contracts</h3>
            <p><ul><?php
    }
    echo "<li><a href='?page=view_contract&contid=$contid'>$contname</a>, issued $timest</li>\n";
}
if (!$first)
    echo "</ul></p></div>";

?>
<p>Use the following form to issue new contracts that you can sell and buy.</p>
<p><b>Warning: issued contracts <i>cannot</i> be revoked or changed. They are permanent.</b></p>
<p>
<form action='?page=view_contract' method='post'>
    <input type='hidden' name='new_contract' value='true' />
    <label for='contname'>Contract name:</label>
    <input type='text' name='contname' value='' />
    <label for='text'>Text of the agreement:</label>
    <textarea name='text'></textarea>
    <input type='submit' value='Issue' />
</form>
</p>

